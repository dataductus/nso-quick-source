# nso-quick-source

NSO Quick Source Tool, that allows user to source ncsrc, access CLI and perform other shortcuts from anywhere within the project structure.
This is a handy tool for developers, that are using multiple terminals, when working with NSO projects. It can also be useful when switching projects.

## Installation

In order to install nso-quick-source, please follow the guidelines, provided below.

* Clone the repository.
* Navigate to the repository with your terminal.
* Run `./setup-bash.sh`, that will modify the `.bashrc` (or `.zshrc`, as ZSH is also supported) file with necessary configuration.

## Technical details

As mentioned in previous section, the `setup-bash.sh` script modifies the configuration of shell's rc file. It primarily adds aliases, that are used to run the `get-project-root.sh` script. This script is used to detect NSO project root, as well as the rc file of the project. The project root is fetched by getting git root of the NSO project. Therefore, the user of the script does not have to be at the project root to use the commands. Running the `get-project-root.sh` generates another script file, called `setenv.sh`, that exports variables, containing project root and the location of RC file. These variables are then used by other aliases, to perform various tasks (described in Usage section).

## Usage

* Sourcing of the RC file can be done by running `src` alias from anywhere within the project structure.
* NSO/netsim CLI can be accessed by running `nso-cli <docker-container-name>` from anywhere within the project structure (this can be executed without having to run the `src` command beforehand).
* NSO test environment can be set up by running `nso-setup-env` from anywhere within the project structure (this can be executed without having to run the `src` command beforehand).

The `terminalrc` file contains some other useful aliases. Contributors are encouraged to add more aliases to this file.

