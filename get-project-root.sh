#!/bin/bash

CURRENT="$PWD"

if [ -z "$QUICK_SOURCE_FOLDER_PATH" ]; then

	echo "NSO script folder path variable is not set, please set QUICK_SOURCE_FOLDER_PATH variable"

else

	SCRIPT_PATH="$QUICK_SOURCE_FOLDER_PATH"
	echo "$SCRIPT_PATH"

	SET_ENV_SCRIPT_PATH="${SCRIPT_PATH}/setenv.sh"
	rm -rf "$SET_ENV_SCRIPT_PATH"

	# Fetch project root
	PROJ_DIR=$(git rev-parse --show-toplevel)

	if [ "$PROJ_DIR" == "fatal: not a git repository (or any of the parent directories): .git" ]; then

		echo "Project directory was not found"

	else

		echo "Project directory was found: ${PROJ_DIR}"

		# Get all files, that end with "rc" and check if 1 found
		SRC_FILES="${PROJ_DIR}/*rc"
		LEN=${#SRC_FILES[@]}

		if [ "$LEN" == "1" ]; then

			for SRC in $SRC_FILES; do

				echo "RC file was located: ${SRC}"

	  			#source "$SRC"
	  			#make -C "$PROJ_DIR" clean all setup-basic-test-env
			done

			# Write a shell script, that sets environment variables
			echo "Writing shell script with variables NSO_PROJECT_PATH and NSO_SRC_PATH"
			echo "Shell script path: ${SET_ENV_SCRIPT_PATH}"
			
			touch "$SET_ENV_SCRIPT_PATH"

			echo "#!/bin/bash" >> "$SET_ENV_SCRIPT_PATH"
			echo "export NSO_PROJECT_PATH=${PROJ_DIR}" >> "$SET_ENV_SCRIPT_PATH"
			echo "export NSO_SRC_PATH=${SRC}" >> "$SET_ENV_SCRIPT_PATH"

			chmod a+x "$SET_ENV_SCRIPT_PATH"

		else

			echo "RC file was not located"
			
		fi

	fi
	
fi