#!/bin/bash

if [ -z "$QUICK_SOURCE_FOLDER_PATH" ]; then

    if  [[ $(echo "$SHELL") == *"zsh" ]]; then

        echo "Setting up for zsh"
        BASH_RC_FILE="${HOME}/.zshrc"

    else

        echo "Setting up for bash"
        BASH_RC_FILE="${HOME}/.bashrc"

    fi

    DIR_PATH=$(git rev-parse --show-toplevel)

    
    echo "" >> "$BASH_RC_FILE"
    echo "# Variables for NSO Quick-Source script" >> "$BASH_RC_FILE"
    echo "export QUICK_SOURCE_FOLDER_PATH=${DIR_PATH}" >> "$BASH_RC_FILE"
    echo "source \${QUICK_SOURCE_FOLDER_PATH}/terminalrc" >> "$BASH_RC_FILE"

    # Restart shell
    if  [[ $(echo "$SHELL") == *"zsh" ]]; then

        # exec zsh
        echo "Please make sure to restart your existing shells for the changes to take an effect!"

    else

        # exec bash
        echo "Please make sure to restart your existing shells for the changes to take an effect!"

    fi

else

    echo "Quick Source parameters are already sourced"

fi